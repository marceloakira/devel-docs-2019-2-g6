﻿
# Equipe da Documentação do Desenvolvedor

View docs: [Docs Tetrinet](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/tree/master/Files)

----

## Objetivos:
* Criar/Melhorar diagramas de sequência
    * https://pt.wikipedia.org/wiki/Diagrama_de_sequ%C3%AAncia
* Modelar o protocolo do jogo tetrinet
* Documentar o projeto do tetrinetjs sobre o ponto de vista do desenvolvedor
* Desenho a nivel arquitetural(Atual/Desejado)
    * [Diagrama de componentes](knowledgecenter/pt-br/SS8PJ7_9.1.1/com.ibm.xtools.modeler.doc/topics/twrkcompd.html)

## Grupo 6 - membros
* ROGÉRIO AMORIM MARINHO JUNIOR (Líder)
* THIAGO NONATO DE SOUZA (Dev)
* JHONATA LEANDRO BERNADES PAIVA (Dev)
* ISMAEL JOSÉ CIRILO JUNIOR (DeV)
* TONY  

## Tarefas da sprint 1 (24/11 a 30/11): 
* Corrigir diagramas de sequência já existentes
* Instalar o jogo (cliente e servidor) e realizar testes
* Criar diagramas de sequência restantes
* Sugestão para criar o diagrama de sequência: usar o iplantuml/jupyter
    * https://gitlab.com/marceloakira/tutorial/tree/master/diagramas-de-sequencia


[Ismael](https://gitlab.com/ismaeljcjunior)   | Diagramas
--------- | ------
Diagrama REQ08 | [Diagrama](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/blob/master/Files/Diagrama%20de%20Sequencia/Requisito%2008/Diagrama-Requisito08.png)
Diagrama REQ09 | [Diagrama](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/commit/065efaad4d8092fdd55139c770de70bd68d8deb8)
Diagrama REQ09 | [Diagrama](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/commit/065efaad4d8092fdd55139c770de70bd68d8deb8)
Diagrama REQ11 | [Diagrama](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/blob/6d46935902acb9ec2c76a8b259943bef6ea603f7/Files/Diagrama%20de%20Sequencia/Requisito%2011/Diagrama-Requisito11.png)
Diagrama REQ12 | [Diagrama](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/blob/6d46935902acb9ec2c76a8b259943bef6ea603f7/Files/Diagrama%20de%20Sequencia/Requisito%2012/Diagrama-Requisito12.png)
Diagrama REQ13 | [Diagrama](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/blob/6d46935902acb9ec2c76a8b259943bef6ea603f7/Files/Diagrama%20de%20Sequencia/Requisito%2013/Diagrama-Requisito13.png)
Diagrama REQ14 | [Diagrama](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/blob/6d46935902acb9ec2c76a8b259943bef6ea603f7/Files/Diagrama%20de%20Sequencia/Requisito%2014/Diagrama-Requisito14.png)
Diagrama REQ15 | [Diagrama](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/blob/6d46935902acb9ec2c76a8b259943bef6ea603f7/Files/Diagrama%20de%20Sequencia/Requisito%2015/Diagrama-Requisito15.png)
Diagrama REQ24 | [Diagrama](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/blob/6d46935902acb9ec2c76a8b259943bef6ea603f7/Files/Diagrama%20de%20Sequencia/Requisito%2024/Diagrama-Requisito24.png)
Diagrama REQ24 | [Diagrama](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/blob/6d46935902acb9ec2c76a8b259943bef6ea603f7/Files/Diagrama%20de%20Sequencia/Requisito%2024/Diagrama-Requisito24.png)
Diagrama REQ25 | [Diagrama](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/blob/6d46935902acb9ec2c76a8b259943bef6ea603f7/Files/Diagrama%20de%20Sequencia/Requisito%2025/Diagrama-Requisito25.png)
Diagrama REQ26 | [Diagrama](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/blob/6d46935902acb9ec2c76a8b259943bef6ea603f7/Files/Diagrama%20de%20Sequencia/Requisito%2026/Diagrama-Requisito26.png)
Diagrama REQ27 | [Diagrama](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/blob/6d46935902acb9ec2c76a8b259943bef6ea603f7/Files/Diagrama%20de%20Sequencia/Requisito%2027/Diagrama-Requisito27.png)
Diagrama REQ28 | [Diagrama](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/blob/6d46935902acb9ec2c76a8b259943bef6ea603f7/Files/Diagrama%20de%20Sequencia/Requisito%2028/Diagrama-Requisito28.png)


[Thiago](https://gitlab.com/thiaguimdois)   | Diagramas
--------- | ------
Diagrama REQ04 | [Diagrama](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/blob/master/Files/Diagrama%20de%20Sequencia/Requisito%204/Diagrama_de_Sequencia_04.png)
Diagrama REQ06 | [Diagrama](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/commit/006b1e43149b17c9f18158652ee09880377ed7c7#2e86b54a955de4e21b27ec9d7c39e0f3603a3071)
Diagrama REQ05 | [Diagrama](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/commit/906e8826f62f4aabf9ae5865d78e23767a3da814)
Diagrama REQ03 | [Diagrama](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/commit/6e9eb57b8cb7da63b67688d17aa31824774477ed)


[Tony](https://gitlab.com/tonymfreitas)   | Diagramas
--------- | ------



[Jhonata](https://gitlab.com/jhonleandro)   | Diagramas
--------- | ------
Diagrama REQ07 | [Diagrama](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/commit/028d5165063713d02ccb82878383ee4bab4b196e)
Diagrama REQ18 | [Diagrama](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/commit/7c046091b477aba3f91045360524ea2864d4a0ab)
Diagrama REQ23   | [Diagrama](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/blob/7ef12d59411aa945ab0438793575be64deb80be4/Files/Diagrama%20de%20Sequencia/Requisito%2023/Requisito%2023%20-%20Finalizar%20Jogo.png)



[Rogério](https://gitlab.com/RogerioAmorim)   | Documento RFC-Tetrinet
--------- | ------
Documento RFC | [RFCv01](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/blob/bd057108277c1892ea09f6bfe25b473b4b81d7cc/Files/RFC.pdf)
Documento RFC | [RFCv02](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/blob/896ce44e9ce7b37220dcb24e2678d19ec55d6939/Files/RFC.pdf)
Documento RFC | [RFCv03](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/blob/e4b60dbbd76768591da6dc5475c0ac3c789c350c/Files/RFC.pdf)
Documento RFC | [RFCv04](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/blob/632f585e4682053f83410a597e590b0e1be6f113/Files/RFC.pdf)


## Tarefas da sprint 2 (01/12 a 7/12): 
* Ajuste de todas as issues
* Desenho da arquitetura do software atual (Diagramas de componentes )(FEITO) ROGERIO
* Criar documento de RFC tetrinets v2.0
* Criar documento justificando a mudança da arquitetura(FOCAR)
* Criar diagramas de sequencia demonstrando as falhas do software(FOCAR)
* Criar e documentar diagrama de componentes da arquitetura deseja(FOCAR)




AUTENTICAÇÂO

garbage colection 

LATENCIA -> SEGURANÇA
SEGURANÇA -> LATENCIA
