﻿
#  Requisito 22 - Pausar Jogo
## 1 - Resumo
**História de Usuário**
>Como Jogador, posso pausar o jogo.

**Pré-condições**<br />
>1.1. Deve haver uma partida ativa.<br/>
>1.2. O jogador deve ser moderador.<br/>

**Pós-condições**
>1. **Fluxo Normal** <br />
    1.1. O moderador pressiona a tecla de pausa do jogo, então a partida é pausada para todos os jogadores;<br/>
    1.2. O moderador pressiona a tecla de pausa do jogo, então a partida volta a ser executada.<br/>

>2. **Fluxo Alternativo** <br/>
    2.1. Se não houver partida ativa, o jogo não é pausado.<br/>
    2.2. Caso o jogador não seja moderador, então o jogo não é pausado.<br/>    


**Observações**
>Quando o moderador solicita o pause, então toda a partida é congelada até o moderador retirar o pause.
## 2 - Desenvolvedor
**Protocolo**
>TCP

**Mensagens**
###### Observação: Todas as mensagens são enviadas no formato Hexadecimal.


>1. **Enviadas para o Servidor** <br/>
> --- **Codificação**: Essa mensagem **não é codificada**!<br/>
> --- **Mensagem enviada**: **pause < pause >< iDJogador >**<br/>
> --- **Parâmetros**:<br/>
> --------- pause:number => Se pausa ou despausa o jogo (1 = pause ou 0= despause);<br/>
> --------- iDJogador:number => Id do jogador;<br/>
   
> **Exemplo de envio**:<br/>
 > --------- em **Hexadecimal**: <br/>706175736520312031ff<br/>
> --------- em **Texto imprimivel**: pause 1 1ÿ<br/>

>2. **Recebidas do Servidor** <br/>
>**Observação**: O servidor envia essa mensagem para todos os clientes ativos, através de **comunicação sockets**<br/>
> --- **Codificação**: Essa mensagem **não é codificada**!<br/>
> --- **Mensagem enviada**: **pause < pause >**<br/>
> --- **Parâmetros**:<br/>
> --------- pause:number => Se pausa ou despausa o jogo (1 = pause ou 0= despause);<br/>
   
> **Exemplo de envio**:<br/>
 > --------- em **Hexadecimal**: 70617573652031ff<br/>
> --------- em **Texto imprimivel**: pause 1ÿ<br/>
## 3 - Diagrama de sequência

![Requisito 20](https://gitlab.com/tetrinetjs/devel-docs/raw/jonathas/Files/Diagrama%20de%20Sequencia/Requisito%2022/Requisito%2022%20-%20Pausar%20Jogo.png)



