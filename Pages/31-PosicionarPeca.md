﻿#  Requisito 31 - Posicionar Peça
## 1 - Resumo
**História de Usuário**
>Como jogador, quero posicionar uma peça.

**Pré-condições**<br />
>A peça está encosto no topo de outra peça.

**Pós-condições**
>1. **Fluxo Normal** <br />
    1.1. A peça é posicionada ao topo de outra peça, então aparece outra peça para ser posicionada.<br/>

>2. **Fluxo Alternativo** <br/>
	2.1 - Se a partida encerrar, não há como posicionar mais peças nessa partida. <br/>


**Observações**
>  

Quando uma peça encosta no topo de outra peça, o jogo já envia outra peça para o jogador encaixar em algum lugar.

## 2 - Desenvolvedor
**Protocolo**
>TCP

**Mensagens**
###### Observação: Todas as mensagens são enviadas no formato Hexadecimal.


>1. **Enviadas para o Servidor** <br/>
> --- **Codificação**: Essa mensagem **não é codificada**!<br/>
> --- **Mensagem enviada**: **f < iDJogador > < tipoPeca >< posicao >**<br/>
> --- **Parâmetros**:<br/>
> --------- idJogador:number => Numero identificador do jogador;<br/>
> --------- tipoPeca:string =>  " =>![Tabela Peças](https://gitlab.com/tetrinetjs/devel-docs/raw/jonathas/imgs/Tabela%20de%20pe%C3%A7as.png)<br/>
> --------- posicao:string =>  " =>![Tabela Peças](https://gitlab.com/tetrinetjs/devel-docs/raw/jonathas/imgs/Tabuleiro.png)<br/>
>;<br/>
   
> **Exemplo de envio**:<br/>
> --------- em **Hexadecimal**: 6620312023342047334834483548**ff**<br/>
> --------- em **Texto imprimivel**: f 1 #4 G3H4H5H**ÿ**<br/>

>2. **Recebidas do Servidor** <br/>
>**Observação**: O servidor envia essa mensagem para todos os clientes ativos, através de **comunicação sockets**<br/>
> --- **Codificação**: Essa mensagem **não é codificada**!<br/>
> --- **Mensagem enviada**: **f < iDJogador > < tipoPeca >< posicao >**<br/>
> --- **Parâmetros**:<br/>
> --------- idJogador:number => Numero identificador do jogador;<br/>
> --------- tipoPeca:string =>  " =>![Tabela Peças](https://gitlab.com/tetrinetjs/devel-docs/raw/jonathas/imgs/Tabela%20de%20pe%C3%A7as.png)<br/>
> --------- posicao:string =>  " =>![Tabela Peças](https://gitlab.com/tetrinetjs/devel-docs/raw/jonathas/imgs/Tabuleiro.png)<br/>
>;<br/>
   
> **Exemplo de envio**:<br/>
> --------- em **Hexadecimal**: 6620312023342047334834483548<br/>
> --------- em **Texto imprimivel**: f 1 #4 G3H4H5H<br/>
## 3 - Diagrama de sequência

![Requisito 31](https://gitlab.com/tetrinetjs/devel-docs/raw/jonathas/Files/Diagrama%20de%20Sequencia/Requisito%2031/Requisito%2031%20-%20Posicionar%20Pe%C3%A7a.png)



