﻿﻿
#  Requisito 18 - Lista de Vencedores

## 1 - Resumo
**História de Usuário**
>Como usuário, gostaria de visualizar a lista de vencedores do jogo.

**Pré-condições**
>Uma partida deve ter finalizado com sucesso, ou seja, definido um vencedor.

**Pós-condições**
>1. **Fluxo Normal** <br />
    1.1. O jogo exibe o vencedores, caso nenhuma partida tenha sido realizada, não irá aparecer nenhum vencedor, apenas uma lista vazia.<br/>

>2. **Fluxo Alternativo** <br/>
    2.1. Caso o moderador cancela a partida, não é realizado a nova contagem de pontos;<br/>


**Observações**
>O usuário tem a opção de consultar uma lista de vencedores. Caso nenhuma partida tenha sido finalizada com sucesso anteriormente, então será exibido uma lista vazia de vencedores.
## 2 - Desenvolvedor
**Protocolo**
>TCP

**Mensagens**
###### Observação: Todas as mensagens são enviadas no formato Hexadecimal.

>1. **Enviadas para o Servidor** <br/>
> Nenhuma mensagem é enviada

>2. **Recebidas do Servidor** <br/>
> --- **Codificação**: Essa mensagem **não é codificada**!<br/>
> --- **Mensagem enviada**: **winlist  List{< type >< nickname >;< points >}ÿ**<br/>
> --- **Parâmetros**:<br/>
> --------- type:char => Qual o tipo do vencedor (t = Team ou p = Player)<br/>
> --------- nickname:string => Nickname do Jogador/Time<br/>
> --------- points:number => Pontuação do Jogador/Time <br/>
   
> **Exemplo de envio**:<br/>
 > --------- em **Hexadecimal**: <br/>77696e6c6973742074546f757254686541627973733b39323220704672656767653b3835352070656c697373613b343735**ff** (Ao final da string adiciona 'ff')<br/>
> --------- em **Texto imprimivel**: winlist tTourTheAbyss;922 pFregge;855 pelissa;475ÿ<br/>
## 3 - Diagrama de sequência

![Requisito 18](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/blob/master/Files/Diagrama%20de%20Sequencia/Requisito%2018/Requisito%2018%20-%20Lista%20de%20Vencedores.png)
