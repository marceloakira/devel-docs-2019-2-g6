﻿

#  Requisito 7 - Aplicar especial

## 1 - Resumo
**História de Usuário**
>Como jogador eu quero usar um especial na partida.

**Pré-condições**
>1. O jogador tem que ter um especial na fila de especiais.  <br />
>2. O jogador pressiona a tecla correspondente ao especial e depois aperta uma tecla numérica corresponde a algum jogador;<br />

**Pós-condições**
>1. **Fluxo Normal** <br />
    1.1. O jogador quer aplicar um especial, que foi conquistado e está na fila de especiais, então aperta a tecla: atalho do especial + número identifcador do jogador, os números vão de 1 a 6;<br/>

>2. **Fluxo Alternativo** <br/>
	2.1 - O jogador informa um identificador(numero) inválido, então o especial não é aplicado;  <br/>
	2.2 - O jogador informa a tecla(letra) errada inválida para o especial, então o especial não é aplicado;<br/>

**Observações**
>![Especiais](https://gitlab.com/tetrinetjs/devel-docs/raw/jonathas/imgs/Especiais.png)
## 2 - Desenvolvedor
**Protocolo**
>TCP

**Mensagens**
###### Observação: Todas as mensagens são enviadas no formato Hexadecimal.

>1. **Enviadas para o Servidor** <br/>
> --- **Codificação**: Essa mensagem **não é codificada**!<br/>
> --- **Mensagem enviada**: **sb < iDJogadorAlvo > < especial > < iDJogadorOrigem >**<br/>
> --- **Parâmetros**:<br/>
> --------- iDJogadorAlvo:string => Id do jogador que será aplicado o especial ;<br/>
> --------- especial:string => referência do especial (a, c, n, r, s, b, g, q, o) ;<br/>
> --------- iDJogadorOrigem:string => Id do jogador que enviou o especial ;<br/>
   
> **Exemplo de envio**:<br/>
 > --------- em **Hexadecimal**: <br/>7362203120612031**ff** (Ao final da string adiciona 'ff')<br/>
> --------- em **Texto imprimivel**: sb 1 a 1ÿ<br/>

>2. **Recebidas do Servidor** <br/>
>**Observação**: O servidor envia essa mensagem para todos os clientes ativos, através de **comunicação sockets**<br/>
> --- **Codificação**: Essa mensagem **não é codificada**!<br/>
> --- **Mensagem enviada**: **sb < iDJogadorAlvo > < especial > < iDJogadorOrigem >**<br/>
> --- **Parâmetros**:<br/>
> --------- iDJogadorAlvo:string => Id do jogador que será aplicado o especial ;<br/>
> --------- especial:string => referência do especial (a, c, n, r, s, b, g, q, o) ;<br/>
> --------- iDJogadorOrigem:string => Id do jogador que enviou o especial ;<br/>
   
> **Exemplo de envio**:<br/>
 > --------- em **Hexadecimal**: <br/>7362203120612031**ff** (Ao final da string adiciona 'ff')<br/>
> --------- em **Texto imprimivel**: sb 1 a 1ÿ<br/>
## 3 - Diagrama de sequência

![Requisito 7](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/raw/master/Files/Diagrama%20de%20Sequencia/Requisito%207/Requisito%207%20-%20Aplicar%20Especial.png)



