﻿

#  Requisito 28 - Bater papo na partida
## 1 - Resumo
**História de Usuário**
>  Como jogador, eu quero enviar mensagem apenas para os jogadores da partida.

**Pré-condições**<br /> 
>1. Estar em uma partida ativa;  <br /> 
>2. Pressionar a tecla "t", para abrir o batepapo e enviar uma mensagem.<br /> 

**Pós-condições**
>1. **Fluxo Normal** <br />
    1.1. Um jogador pressiona a tecla "t" para abrir o batepapo, e envia uma mensagem para todos os jogadores da partida..<br/>

>2. **Fluxo Alternativo** <br/>
	2.1 - Caso o usuário saia da partida, ele não recebe a mensagem. <br/>
	2.2 - Caso não haja uma partida em andamento, o jogador não pode usar o batepapo da partida. <br/>


**Observações**
> O Jogador pode enviar mensagens no batepapo da partida apenas durante a partida. Caso o jogo encerre e inicie outro, o batepapo é limpo.

## 2 - Desenvolvedor
**Protocolo**
>TCP

**Mensagens**
###### Observação: Todas as mensagens são enviadas no formato Hexadecimal.


>1. **Enviadas para o Servidor** <br/>
> --- **Codificação**: Essa mensagem **não é codificada**!<br/>
> --- **Mensagem enviada**: **gmsg < nickname > < mensagem >**<br/>
> --- **Parâmetros**:<br/>
> --------- nickname:string => Numero identificador do jogador;<br/>
> --------- mensagem:string => Mensagem a ser enviada;<br/>
   
> **Exemplo de envio**:<br/>
 > --------- em **Hexadecimal**: 676d7367203c6774657472696e6574353e20566f752067616e68617221**ff**<br/>
> --------- em **Texto imprimivel**: gmsg < gtetrinet5 > Vou ganhar!ÿ<br/>

>2. **Recebidas do Servidor** <br/>
>**Observação**: O servidor envia essa mensagem para todos os clientes ativos, através de **comunicação sockets**<br/>
> --- **Codificação**: Essa mensagem **não é codificada**!<br/>
> --- **Mensagem enviada**: **gmsg < nickname > < mensagem >**<br/>
> --- **Parâmetros**:<br/>
> --------- nickname:string => Numero identificador do jogador;<br/>
> --------- mensagem:string => Mensagem a ser enviada;<br/>
   
> **Exemplo de envio**:<br/>
 > --------- em **Hexadecimal**: 676d7367203c6774657472696e6574353e20566f752067616e68617221<br/>
> --------- em **Texto imprimivel**: gmsg < gtetrinet5 > Vou ganhar!<br/>
## 3 - Diagrama de sequência

![Requisito 28](https://gitlab.com/tetrinetjs/devel-docs/raw/jonathas/Files/Diagrama%20de%20Sequencia/Requisito%2028/Requisito%2028%20-%20Bate%20Papo%20Partida.png)



