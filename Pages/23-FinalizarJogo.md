﻿
#  Requisito 23 - Finalizar Jogo
## 1 - Resumo
**História de Usuário**
>Como Jogador quero finalizar o jogo.

**Pré-condições**<br />
>1.1. Deve haver uma partida ativa.<br/>
>1.2. O jogador deve ser moderador.<br/>

**Pós-condições**
>1. **Fluxo Normal** <br />
    1.1. O moderador pressiona a tecla de parar do jogo, então a partida é finalizada para todos os jogadores.;<br/>

>2. **Fluxo Alternativo** <br/>
    2.1. Se não houver partida ativa, o jogo não é finalizado.<br/>
    2.2. Caso o jogador não seja moderador, então o jogo não é finalizado.<br/>    


**Observações**
>Quando o moderador solicita a finalização da partida, então toda a partida é finalizada e a pontuação não é computada na lista de vencedores..
## 2 - Desenvolvedor
**Protocolo**
>TCP

**Mensagens**
###### Observação: Todas as mensagens são enviadas no formato Hexadecimal.


>1. **Enviadas para o Servidor** <br/>
> --- **Codificação**: Essa mensagem **não é codificada**!<br/>
> --- **Mensagem enviada**: **startgame < start >< iDJogador >**<br/>
> --- **Parâmetros**:<br/>
> --------- start :number => Se inicia ou finaliza o jogo (1 = inicia ou 0 = finaliza);<br/>
> --------- iDJogador:number => Id do jogador;<br/>
   
> **Exemplo de envio**:<br/>
 > --------- em **Hexadecimal**: <br/>4542656d4040d746ebe87ae1553178e13fcfc5efe36ae0acbe0a737461727467616d6520302031ff<br/>
> --------- em **Texto imprimivel**: startgame 0 1ÿ<br/>

>2. **Recebidas do Servidor** <br/>
>**Observação**: O servidor envia essa mensagem para todos os clientes ativos, através de **comunicação sockets**<br/>
> --- **Codificação**: Essa mensagem **não é codificada**!<br/>
> --- **Mensagem enviada**: **endgame**<br/>
<br/>
   
> **Exemplo de envio**:<br/>
 > --------- em **Hexadecimal**: 656e6467616d65<br/>
> --------- em **Texto imprimivel**: endgame<br/>
## 3 - Diagrama de sequência

![Requisito 23](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/raw/master/Files/Diagrama%20de%20Sequencia/Requisito%2023/Requisito%2023%20-%20Finalizar%20Jogo.png)



