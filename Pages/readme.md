

# Guia de navegacao

Documenta��o produzida em  **MarkDown**

----

## Prefacio:
>Os requisitos estao organizados em um arquivo individualmente. <br />
Cada arquivo de requisito segue a seguinte estrutura:
	
    1-Historia de usu�rio e Caso de uso;
    2-Documenta��o para o desenvolvedor;
    3-Diagrama de sequencia;
    

## Requisitos
> ### Requisitos que trocam mensagem com o servidor:

[1 - Conectar com Servidor](https://gitlab.com/tetrinetjs/devel-docs/blob/jonathas/Pages/1-ConectarServidor.md)<br />
[2 - Iniciar Partida](https://gitlab.com/tetrinetjs/devel-docs/blob/jonathas/Pages/2-IniciarPartida.md)<br />
[7 - Aplicar Especiais](https://gitlab.com/tetrinetjs/devel-docs/blob/jonathas/Pages/7-AplicarEspeciais.md)<br />
[16 - Bate-Papo Canal principal](https://gitlab.com/tetrinetjs/devel-docs/blob/jonathas/Pages/16-BatePapoCanalPrincipal.md)<br />
[17 - EfetuarLogoff](https://gitlab.com/tetrinetjs/devel-docs/blob/jonathas/Pages/17-EfetuarLogoff.md)<br />
[18 - Lista de Vencedores](https://gitlab.com/tetrinetjs/devel-docs/blob/jonathas/Pages/18-ListaVencedores.md)<br />
[19 - Participar de Time](https://gitlab.com/tetrinetjs/devel-docs/blob/jonathas/Pages/19-ParticiparTime.md)<br />
[20 - Vencer Jogo](https://gitlab.com/tetrinetjs/devel-docs/blob/jonathas/Pages/20-VencerJogo.md)<br />
[21 - Perder Jogo](https://gitlab.com/tetrinetjs/devel-docs/blob/jonathas/Pages/21-PerderJogo.md)<br />
[22 - Pausar Jogo](https://gitlab.com/tetrinetjs/devel-docs/blob/jonathas/Pages/22-PausarJogo.md)<br />
[23 - Finalizar Jogo](https://gitlab.com/tetrinetjs/devel-docs/blob/jonathas/Pages/23-FinalizarJogo.md)<br />
[28 - Bate-Papo na Partida](https://gitlab.com/tetrinetjs/devel-docs/blob/jonathas/Pages/28-BatePapoPartida.md)<br />
[31 - Posicionar Peca](https://gitlab.com/tetrinetjs/devel-docs/blob/jonathas/Pages/31-PosicionarPeca.md)<br />

## Utilitario:
[Decode](https://gitlab.com/tetrinetjs/devel-docs/blob/jonathas/Pages/Utils/Decode.md)<br />
[Encode](https://gitlab.com/tetrinetjs/devel-docs/blob/jonathas/Pages/Utils/Encode.md)<br />
## Referencias:
*  [Planilha de Requisitos](https://docs.google.com/spreadsheets/d/1aBG3MdXgFSwhUdVcQhnHY4OeS3fhB8Uz0S5xYi-SqEI/edit#gid=0)<br />
*  [Protocolo Tetrinet](https://gitlab.com/marceloakira/devel-docs-2019-2-g6/blob/master/Files/RFC.pdf)<br />