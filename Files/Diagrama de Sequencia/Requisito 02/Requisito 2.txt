@startuml

autonumber
actor Jogador
boundary Cliente

Jogador -> Cliente: Inicia a partida.
Cliente -> Cliente: Prepara o comando: startgame < start >< iDJogador > para enviar para o servidor
Cliente -> Servidor: Envia o comando em hexadecimal.
Servidor -> Cliente : Envia mensagem para o clientes usando a resposta: newgame [...args] f < iDJogador > < campos >  em formato Hexadecimal.
Cliente -> Cliente : Traduz a mensagem de Hexadecimal para String.
Cliente -> Cliente : Configura a partida do usu�rio com as configura��es definidas no servidor
Cliente -> Cliente : Coloca o usu�rio na partida
Cliente -> Jogador : Notifica o usu�rio que a partida j� come�ou!

@enduml
